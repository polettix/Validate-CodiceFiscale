# Archivio Comuni

L'archivio dei comuni dell'ANPR fa parte delle varie [tabelle di decodifica][].

[Link diretto][archivio-comuni].

Salvare il file CSV con il nome `ANPR_archivio_comuni.csv`.

# Archivio Stati Esteri

L'archivio degli stati esteri dell'ISTAT è disponibile nella pagina
[Codici delle unità territoriali estere][istat-esteri].

Il link diretto all'archivio ZIP dovrebbe essere [questo][istat-zip].
Estrarre da esso il file in formato CSV e rinominarlo
`ISTAT_archivio_stati_esteri.csv`.

# Generazione JSON aggregato

Per rigenerare il JSON aggregato, aggiornare i due file CSV suddetti e poi
lanciare il programma `generate_json.sh`.

Il file risultante sarà `territorio.json` nella directory superiore.


[tabelle di decodifica]: https://www.anagrafenazionale.interno.it/area-tecnica/tabelle-di-decodifica/
[archivio-comuni]: https://www.anagrafenazionale.interno.it/wp-content/uploads/ANPR_archivio_comuni.csv
[istat-esteri]: https://www.istat.it/it/archivio/6747
[istat-zip]: https://www.istat.it/it/files//2011/01/Elenco-codici-e-denominazioni-unita-territoriali-estere.zip
