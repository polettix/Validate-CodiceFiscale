#!/bin/sh

set -eu

md="$(dirname "$(readlink -f "$0")")"
rd="$(dirname "$(dirname "$md")")"

#r() { PERL5LIB="$rd/lib:$rd/bundle-lib" perl "$rd/script/romeo" "$@"; }
r() { romeo "$@" ; }

info() { printf >&2 '%s\n' "$*" ; }

archivio_comuni() {
   local file="$1"
   r csv2json --bom -i "$file" \
   | jq '[ .[] | {
      (.codcatastale): {
         name: .denominazione_it,
         start: .dataistituzione,
         end: .datacessazione
         }
      } ]'
}

archivio_stati_esteri() {
   local file="$1"
   iconv -f iso-8859-1 -t utf-8 <"$file" \
   | r csv2json --bom \
   | jq '[ .[] | select(."codice at" != "n.d.") | {
      (."codice at"): {
         name: ."denominazione it",
         start: "0000-01-01",
         end: "9999-12-31"
         }
      } ]'
}

assemble() {
   perl -e '

use strict;
use warnings;
use JSON::PP qw< decode_json encode_json >;

binmode STDIN, ":raw";
local $/;
my $records = decode_json(scalar(<STDIN>));
my %data_for;
for my $record ($records->@*) {
   my ($name, $range) = $record->%*;
   my $slot = $data_for{$name} //= [];
   push $slot->@*, $range;
}

my $encoder = JSON::PP->new->ascii->shrink;
print $encoder->encode(\%data_for), "\n";

   ';
}

newliner() {
   perl -e '

use strict;
use warnings;
use constant LINE_LENGTH => 76;

my $stuff = do {
   binmode STDIN, ":raw";
   local $/;
   readline(STDIN);
};
$stuff =~ s{\s+\z}{}mxs;

binmode STDOUT, ":raw";
local $\;
while ((my $len = length($stuff)) > 0) {
   my $amount = $len > LINE_LENGTH ? LINE_LENGTH : $len;
   my $line = substr($stuff, 0, $amount, "");
   print {*STDOUT} "$line\x{0A}";
}
   '
}

COMUNI="$md/ANPR_archivio_comuni.csv"
STATI_ESTERI="$md/ISTAT_archivio_stati_esteri.csv"

{
   info "getting data from $COMUNI..."
   archivio_comuni "$COMUNI"
   info "getting data from $STATI_ESTERI..."
   archivio_stati_esteri "$STATI_ESTERI"
}  | jq -s 'add' \
   | assemble \
   | tee "$md/../territorio.json" \
   | newliner >"$md/../territorio.jsonish"
